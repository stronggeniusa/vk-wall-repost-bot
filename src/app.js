import axios from 'axios'
import sqlite from 'sqlite';
import config from './config'
//https://www.scriptol.com/sql/sqlite-async-await.php

setTimeout(() => start(), 1000);
setInterval(() => start(), 3600*1000);

async function start () {
    try {
        //конект с базой
        let db = await sqlite.open('base/memory.db');

        //получение списка новостей
        let arPosts = await vk.wallGet();
        //console.log(arPosts)

        let arPostAdd = await vk.searchOldPost(db, arPosts);
        console.log(arPostAdd)
        //Добавление поста в память
        let addPost = await db.all(`INSERT INTO posts (vk_post_id) VALUES (${arPostAdd.id})`);

        await vk.wallPost(arPostAdd);
        //console.log(result)
    } catch (err) {
        console.log(err)
    }



    //let arPost = await db.all(`SELECT * FROM post WHERE vk_post_id=${}`);




}

class vk {
    static async wallGet () {
        try {
            let inUrl = `https://api.vk.com/method/wall.get?owner_id=${config.in.owner_id}&count=${config.in.count}&filter=${config.in.filter}&access_token=${config.token}&v=5.103`;
            console.log(inUrl)

            //запрос новостей
            let inResult = await axios({
                method: 'get',
                url: inUrl,
                headers: {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 YaBrowser/19.12.4.25 Yowser/2.5 Safari/537.36"}
            });
            //console.log(inResult.data)

            if (!inResult.data.response.items.length)
                throw ({msg: 'Стена пустая'});

            let outUrl = `https://api.vk.com/method/wall.get?owner_id=${config.out.owner_id}&count=2&filter=owner&access_token=${config.token}&v=5.103`;
            console.log(outUrl)

            //запрос новостей
            let outResult = await axios({
                method: 'get',
                url: outUrl,
                headers: {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 YaBrowser/19.12.4.25 Yowser/2.5 Safari/537.36"}
            });
            //console.log(outResult.data)

            let date = +new Date();
            date = Math.floor(date / 1000);

            if (
                (outResult.data.response.items[0].date<outResult.data.response.items[1].date) &&
                ((date - outResult.data.response.items[1].date) < config.out.hour * 3600)
            )
                throw ({msg: 'Время с последнего поста еще не истекло / первая запись старше второй', result: false});

            if (
                (outResult.data.response.items[0].date>outResult.data.response.items[1].date) &&
                ((date - outResult.data.response.items[0].date) < config.out.hour * 3600)
            )
                throw ({msg: 'Время с последнего поста еще не истекло'});

            //выводим все загруженные посты
            return inResult.data.response.items;

        } catch (err) {
            throw ({...{msg: 'Работа с загруженным списком'}, ...err});
        }
    }

    //поиск уже добавленого поста
    static async searchOldPost (db, arr) {
        try {
            //переворачиваем массив
            arr = arr.reverse();

            for (let post of arr) {

                let query = `SELECT * FROM posts WHERE vk_post_id=${post.id}`;
                console.log(query)
                let getPost = await db.all(query);
                console.log(getPost)
                if (getPost.length) continue;

                return post;
            }

            throw ({msg: 'Все посты уже добавлены, новых/исходных нет'});
        } catch (err) {
            throw (err);
        }


    }

    static async wallPost (post) {

        let newAttachments = [];

        for (let object of post.attachments) {
            if (object.type === "video") {
                newAttachments[newAttachments.length] = `video${object.video.owner_id}_${object.video.id}`;
            }
            if (object.type === "photo") {
                newAttachments[newAttachments.length] = `photo${object.photo.owner_id}_${object.photo.id}`;
            }
            if (object.type === "link") {
                newAttachments[newAttachments.length] = object.link.url;
            }
            if (object.type === "article") {
                newAttachments[newAttachments.length] = `article${object.article.owner_id}_${object.article.id}`;
            }
        }

        let newPost = {
            text: encodeURI(post.text),
            attachments: newAttachments.join(',')
        };


        let url = `https://api.vk.com/method/wall.post?owner_id=${config.out.owner_id}&friends_only=${config.out.friends_only}&from_group=${config.out.from_group}&message=${newPost.text}&attachments=${newPost.attachments}&access_token=${config.token}&v=5.103`;
        console.log(url)

        //запрос новостей
        let result = await axios({
            method: 'post',
            url: url,
            headers: {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 YaBrowser/19.12.4.25 Yowser/2.5 Safari/537.36"},

        });


        console.log(result.data)
    }
}